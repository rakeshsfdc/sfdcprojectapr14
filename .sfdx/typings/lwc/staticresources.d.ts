declare module "@salesforce/resourceUrl/Escalated" {
    var Escalated: string;
    export default Escalated;
}
declare module "@salesforce/resourceUrl/Nonescalated" {
    var Nonescalated: string;
    export default Nonescalated;
}
declare module "@salesforce/resourceUrl/SiteSamples" {
    var SiteSamples: string;
    export default SiteSamples;
}
declare module "@salesforce/resourceUrl/ToBeEscalated" {
    var ToBeEscalated: string;
    export default ToBeEscalated;
}
