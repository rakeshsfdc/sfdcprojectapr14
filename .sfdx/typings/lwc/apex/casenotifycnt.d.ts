declare module "@salesforce/apex/casenotifycnt.notifycmpteam" {
  export default function notifycmpteam(param: {recid: any}): Promise<any>;
}
declare module "@salesforce/apex/casenotifycnt.userprofile" {
  export default function userprofile(): Promise<any>;
}
