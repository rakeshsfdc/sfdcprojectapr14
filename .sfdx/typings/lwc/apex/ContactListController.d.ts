declare module "@salesforce/apex/ContactListController.getContactList" {
  export default function getContactList(param: {accountIds: any}): Promise<any>;
}
