declare module "@salesforce/apex/EmployeeData.insertEmployee" {
  export default function insertEmployee(param: {Empl: any}): Promise<any>;
}
declare module "@salesforce/apex/EmployeeData.countryvalues" {
  export default function countryvalues(): Promise<any>;
}
declare module "@salesforce/apex/EmployeeData.detailsEmployees" {
  export default function detailsEmployees(): Promise<any>;
}
declare module "@salesforce/apex/EmployeeData.saveEmployee" {
  export default function saveEmployee(param: {emp: any}): Promise<any>;
}
