declare module "@salesforce/apex/CaseController.accrecords" {
  export default function accrecords(param: {accname: any}): Promise<any>;
}
declare module "@salesforce/apex/CaseController.caserating" {
  export default function caserating(param: {caseid: any, csrating: any}): Promise<any>;
}
declare module "@salesforce/apex/CaseController.skillrating" {
  export default function skillrating(param: {caseid: any, csrating: any}): Promise<any>;
}
declare module "@salesforce/apex/CaseController.techrating" {
  export default function techrating(param: {caseid: any, csrating: any}): Promise<any>;
}
declare module "@salesforce/apex/CaseController.duedeligence" {
  export default function duedeligence(param: {caseid: any, csrating: any}): Promise<any>;
}
declare module "@salesforce/apex/CaseController.fetchcaserating" {
  export default function fetchcaserating(param: {caseid: any, starsType: any}): Promise<any>;
}
declare module "@salesforce/apex/CaseController.fetchtechrating" {
  export default function fetchtechrating(param: {caseid: any}): Promise<any>;
}
