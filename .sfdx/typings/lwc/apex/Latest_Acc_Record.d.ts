declare module "@salesforce/apex/Latest_Acc_Record.laccrecord" {
  export default function laccrecord(): Promise<any>;
}
declare module "@salesforce/apex/Latest_Acc_Record.lconrecord" {
  export default function lconrecord(): Promise<any>;
}
declare module "@salesforce/apex/Latest_Acc_Record.emprecord" {
  export default function emprecord(param: {Name: any, Salary: any}): Promise<any>;
}
