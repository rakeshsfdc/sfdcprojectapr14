public class wrapper2class 
{
    public list<accwrapper>awrapper{set;get;}
    public list<conwrapper>cwrapper{set;get;}
    
    public class accwrapper
    {
        public boolean acheckbox{set;get;}
        public account acc{set;get;}
        
        public accwrapper(boolean tf,account a)
        {
            acheckbox = tf;
            acc = a;
        }
    }  
        public list<accwrapper> getacclist()
        {
           awrapper = new list<accwrapper>();
           list<account>accs =[select name,billingcountry,industry from account order by name limit 10];
           for(account acc :accs)
           {
               accwrapper aws = new accwrapper(false,acc);
               awrapper.add(aws);    
           }
            return awrapper;
           
        }
    
    
    public class conwrapper
    {
        public boolean ccheckbox{set;get;}
        public contact con{set;get;}
        
        public conwrapper(boolean tf, contact c)
        {
            ccheckbox =tf;
            con = c;
        }
    }   
        public list<conwrapper> getconlist()
        {
         	cwrapper =new list<conwrapper>();
            list<contact>cons =[select lastname,email,mailingcountry from contact limit 10];
            for(contact con: cons)
            {
                conwrapper cws = new conwrapper(false,con);
                    cwrapper.add(cws);
            }
            return cwrapper;
        }
    

}