public class Latest_Acc_Record 
{
    public static account acc;
    public static contact con;
	@auraenabled
    public static account laccrecord()
    {
        acc = [select id,name,billingcountry from account order by createddate desc limit 1];
        return acc;
    }
    
    @auraenabled
    public static contact lconrecord()
    {
        con = [select id,name from contact order by createddate desc limit 1];
        return con;
    }
    
    @auraenabled
    public static string emprecord(String Name, Integer Salary)
    {
        employee__c em = new employee__c();
        em.employee_name__c = Name;
        em.salary__c=Salary;
        em.Email__c='test@gmail.com';
        insert em;
        return em.id;
    }
}