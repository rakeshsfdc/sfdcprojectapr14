public class Processselectedcons 
{
    public list<contactwrapper> conwraper{set;get;}
    public list<contact>conlist{set;get;}
    
    public list<contactwrapper> getconwrappers()
    {
        conwraper = new list<contactwrapper>();
        list<contact> cons = [select id,lastname,mailingcountry,phone from contact  order by lastname limit 20];
        for(contact con : cons)
        {
            contactwrapper cw = new contactwrapper(false,con);
            conwraper.add(cw);
        }
        return conwraper;
    }
    
    public void deletcons()
    {
        conlist=new list<contact>();
        for(contactwrapper cw : conwraper)
        {
            if(cw.selected==true)
            {
                conlist.add(cw.con);
            }
        }
        delete conlist;
        conwraper = null;
    }
    
	public class contactwrapper
    {
        public boolean selected{set;get;}
        public contact con{set;get;}
        
        public contactwrapper(boolean sel, contact c)
        {
            this.selected = sel;
            this.con=c;
        }
    }
}