public class Transcton
{
	public integer intrest=1000;
    public integer totalamt;
    public BankingTranscation bt = new BankingTranscation();
    public void totalamount()
    {
       totalamt = bt.balance+intrest;
    }
}

/* private members cannot be accessed outside the class directly(ie through object)
 * private members can be accessed indirectly through setters and getters 

PRIVATE  MEMBERS --- accessible within class
PUBLIC   MEMBERS --- accessible within class and outside the class
PROTECTED        --- Behaves as private in normal classes
					 Behaves as public in case of inheritance
GLOBAL ------------- Accessible within class and outside the class and outside the salesforce
*/