public class CaseHandler
{
	public void updateChildCases(list<case>updatedcaseslist , map<id,case>updatedcasesmap)
    {
        set<id>parentcaseids = new set<id>();
    	list<case>childcases = new list<case>();
		list<case>parentcases = new list<case>();           
    	list<case>updatedchildcases = new list<case>();
    	list<case>updatedparentcases = new list<case>();
    	map<id,case>parentcasesmap = new map<id,case>();
        map<id,case>upcasemap = new map<id,case>();
        map<id,case>casemapparent = new map<id,case>();
       
        
        for(case updatedcase : updatedcaseslist)
        {
            if(updatedcase.Update_Child_Records__c == true)
        	 {
            	parentcaseids.add(updatedcase.id);
        	 }
        }
        childcases = [select id,CaseNumber,parentid from Case where parentid in:parentcaseids];     
        parentcases =[select id,casenumber,Update_Child_Records__c ,status from case where id in: updatedcasesmap.keyset()];
        parentcasesmap.putAll(parentcases);
        system.debug('parentcases--->'+parentcasesmap);
        for(case chcase : childcases)
        {
            case pcase = parentcasesmap.get(chcase.parentid);
            chcase.status = pcase.status;
            pcase.Update_Child_Records__c=false;
           casemapparent.put(pcase.id,pcase);
            updatedchildcases.add(chcase);         
        
        }
        if(casemapparent.size()>0)
        {
            system.debug('up parent map--->'+casemapparent.values());
            update casemapparent.values();
        }
        if(updatedchildcases.size()>0)
        {
            update updatedchildcases;
        }
    
        
    }
    
    public void oldownername(list<case>acclist, map<id,case>oldcasemap, map<id,case>newcasemap)
    {
        set<id>modifiedcaseids = new set<id>();
        map<id,user>oldusers;
        map<id,groupmember>oldusersqueue;
        map<id,id>caseownersmap = new map<id,id>();
        map<id,id>caseownersqueuemap = new map<id,id>();
        list<groupmember>olduserqueuelist = new list<groupmember>();
        
        for(case c : acclist)
         {
             case oc = oldcasemap.get(c.id);
             if(c.ownerid!=oc.ownerid)
             {
                 if(string.valueOf(oc.OwnerId).startsWith('005'))
                 {
             	  caseownersmap.put(c.id,oc.ownerid);               	  
             	 }
                 if(string.valueOf(oc.OwnerId).startsWith('00G'))
                 {
                    caseownersqueuemap.put(c.id,oc.ownerid);  
                     System.debug('queue---'+caseownersqueuemap);
                 }
                 modifiedcaseids.add(c.id);
             }
             
         }
        if(caseownersmap!=null)
        {
          oldusers = new map<id,user>([select id,firstname,lastname from user where id in:caseownersmap.values()]);
        }
        if(caseownersqueuemap!=null)
        {
      	  olduserqueuelist = [select id, group.name,groupid from Groupmember where groupid in:caseownersqueuemap.values()];
        }
   
        oldusersqueue = new map<id,groupmember>();
        for(groupmember g : olduserqueuelist)
        {
            oldusersqueue.put(g.GroupId,g);
        }
            System.debug('queue details--->'+oldusersqueue);
        for(id nid :modifiedcaseids)
        {
            case cs = newcasemap.get(nid);
            case ocs = oldcasemap.get(nid);
            if(string.valueOf(ocs.OwnerId).startsWith('005'))
            cs.oldowner__c = oldusers.get(caseownersmap.get(nid)).firstname +' '+oldusers.get(caseownersmap.get(nid)).lastname;
            else
            cs.oldowner__c = oldusersqueue.get(caseownersqueuemap.get(nid)).group.name;
                
        }
    }
}