public class Wizard 
{
    public account acc{set;get{
        if(acc==null)
        {
            acc =new account();
            return acc;
        }
        else
        {
            return acc;
        }
    }}
     public contact con{set;get{
        if(con==null)
        {
            con =new contact();
            return con;
        }
        else
        {
            return con;
        }
    }}
    
    public pagereference NavigateToconpage()
    {
        pagereference pg = new pagereference('/apex/conPage');
        return pg;
    }
    
    public pagereference accountpage()
    {
        pagereference pg = new pagereference('/apex/Accpage');
        return pg;
    }
    
    public void saverecs()
    {
        insert acc;
        con.AccountId=acc.id;
        insert con;
    }
    
    
}