public class BatchImpl implements Database.Batchable<sobject>,Schedulable
{
    public string query = 'select id,name,billingcountry from account';
    public list<account>uacclist = new list<account>();
	public Database.QueryLocator start(Database.BatchableContext bc)
    {
       return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc ,list<account> acclist)
    {
        for(account acc : acclist)
        {
            if(acc.billingcountry=='INDIA')
            {
                acc.billingcountry='USA';
                uacclist.add(acc);
            }
        }
        
        if(acclist.size()>0)
        {
            update uacclist;
        }
        
    }
    public void finish(Database.BatchableContext bc)
    {
        
    }
    
    public void execute(System.SchedulableContext sc)
    {
        BatchImpl b = new BatchImpl();
		System.schedule('Hourly Batch','0	0	6	*	* ?',new BatchImpl());
    }
}