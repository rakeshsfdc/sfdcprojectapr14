public class EmployeeData
{
    public employee__c emp;
    public EmployeeData()
    {
        //System.debug('Country--'+Employee__c.country__c.getDescribe().getPicklistValues());
    }
    
    @auraenabled
    public static employee__c insertEmployee(employee__c Empl)
    {   
        upsert Empl;
             return Empl;   
    }
    @auraenabled
    public static list<String> countryvalues()
    {
        System.debug('countryvalues()');
        list<string>couvalues = new list<string>();
        list<Schema.PicklistEntry> empcountries = Employee__c.country__c.getDescribe().getPicklistValues();
        for(Schema.PicklistEntry cou : empcountries)
        {
            couvalues.add(cou.getlabel());
        }
                
        return couvalues;
    }
    
    @AuraEnabled
    public static employee__c detailsEmployees(){
        return [Select Id,Name,Employee_Name__c,Salary__c,Country__c,Email__c from employee__c Order By CreatedDate DESC LIMIT 1];
    }
    
    @AuraEnabled
    public static string saveEmployee(employee__c emp)
    {
        insert emp;
        return emp.id;
    }
}