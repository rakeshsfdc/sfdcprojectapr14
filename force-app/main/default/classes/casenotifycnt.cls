public class casenotifycnt
{
@auraenabled
    public static void notifycmpteam(string recid)
    {
        case c = [select id,Notify_Compliance_Team__c from case where id=:recid];
        c.Notify_Compliance_Team__c = true;
        update c;
        
    }
    @auraenabled
    public static string userprofile()
    {
        string pid = userinfo.getProfileId();
        string pname = [select id,name from profile where id=:pid].name;
        return pname;
    }
}