public class EmployeeExtension
{
    Employee__c emp;
	public EmployeeExtension(ApexPages.StandardController std)
    {
        Sobject s = std.getRecord();
        Emp = (Employee__c)s;
    }
    
    public void save()
    {
        try{
            insert emp;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM,'Record Created Successfully'));
           }
        catch(Exception e){
         ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,'Insert Failed'+e.getCause()));
    }
        
    
    }
    
    
}