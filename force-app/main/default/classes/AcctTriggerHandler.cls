public class AcctTriggerHandler
{
    public list<contact>conlist = new list<contact>();
    public list<opportunity>opplist = new list<opportunity>();
    public list<account>upacclist = new list<account>();
    
	public void defaultBillingCountry(list<account>acclist)
    {
        for(Account acc :acclist)
   		 {
			if(acc.BillingCountry==null)
       		 {
         	   acc.BillingCountry = 'INDIA';            
        	 }
    	 } 
    }
    
    public void validatePhone(list<account>acclist)
    {
        for(account acc : acclist)
        {
            if(acc.phone==null)
       		 {
           		 acc.adderror('Account record cant be deleted');
       		 }
        }
    }
    
    public void insertRelatedData(list<account>acclist)
    {
     for(account acc : acclist)
      {
          Contact con = new contact();
          con.lastname = acc.name +' Contact';
          con.AccountId = acc.id; 
         
          conlist.add(con);
          
          Opportunity opp = new opportunity();
          opp.name = acc.name +' Opportunity';
          //opp.StageName='Quotes';
            opp.stagename = Label.Stage;
          opp.CloseDate=System.today()+30;
          opp.AccountId = acc.id;
          opplist.add(opp);
          
      }
      
      if(conlist.size()>0)
      {
          insert conlist;
      }
      if(opplist.size()>0)
      {
          insert opplist;
      }
    }
    
    public void uniqueaccountrecs(list<account>acclist)
    {
        Set<string>accnames = new set<string>();
        Set<string>daccnames = new set<string>();
        for(account acc : acclist) //  HCL,TCS, MindTree
        {
           accnames.add(acc.name);
        }
        list<account>dacclist = [select id,name from account where name in:accnames];
        for(account acc : dacclist)
        {
            daccnames.add(acc.name);
        }
        
        for(account acc : acclist)
        {
           if(daccnames.contains(acc.name))
           {
                acc.name.adderror('Account already exist');
           }            
        }
    }
    
    public void validatebillingcountry(list<account>Nacclist,map<id,account>oldmap)
    {
        for(account acc : Nacclist)
        {
            Account oacc = oldmap.get(acc.id);
            if(acc.billingcountry!=oacc.BillingCountry)
            {
                 acc.billingcountry.adderror('Billing country cant be modified');
            }            
        }
    }
    
    public void rollupaction(map<id,account>accmap)
    {   
        if(RecursiveHandler.runonce())
        {
        list<account>accrelatedcons = [select id,name,industry,(select id,name,accountid from contacts)
                                       from account where id in:accmap.keyset()];
        
        set<id>accountids = new set<id>();
        map<id,list<contact>> accconsmap = new map<id,list<contact>>();
		for(account acc : accrelatedcons)
        {
            accountids.add(acc.id);
        }
        list<contact>upaccrelcons = [select id,name,accountid from contact where accountid in:accountids];
        for(contact con : upaccrelcons)
        {
            list<contact> conlist;
            if(!accconsmap.containsKey(con.accountid))
            {
               conlist  = new list<contact>();
                conlist.add(con);
                accconsmap.put(con.accountid,conlist);
            }
            else
            {
               conlist =  accconsmap.get(con.accountid);
               conlist.add(con);
               accconsmap.put(con.accountid,conlist);
            }
        }
        for(account acc : accrelatedcons)
        {
                list<contact>cons= accconsmap.get(acc.id);                
                if(cons!=null)
                {
               	 acc.Contacts__c = cons.size();
                 upacclist.add(acc);
        		}    
        }
        update upacclist;
       }
    }
}