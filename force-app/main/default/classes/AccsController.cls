public class AccsController 
{
    public string search{set;get;}
    public list<account>acclist{set;get;}
    public string accid{set;get;}
    
    public void accrecs()
    {
        if(search!=null)
        {
        	acclist = [select id,name,industry,billingcountry from account where name  LIKE :('%' +search+ '%') ];
        }        
        else
        {
            acclist=null;
        }
    }
    
    public pagereference contactpage()
    {
        pagereference pg = new pagereference('/apex/Accountrelatedrecords?actid='+accid);
        pg.setRedirect(true);
        return pg;
        
    }
    
}