public class CaseController 
{
	public case casercd;
    
    public casecontroller()
    {
        if(casercd==null)
        {
            casercd = new case();
        }
    }
    
    @auraenabled
    public static list<account> accrecords(string accname)
    {
        list<account>accs = [select id,name from account where name like: '%'+accname+'%']; 
        System.debug('name--'+accs);
        return accs;
    }
    
     @auraenabled
    public static void caserating(string caseid,string csrating)
    {
        list<case> cs = [select id,caserating__c from case where id=:caseid limit 1]; 
        cs[0].caserating__c=csrating;
        update cs;
    }
    
    @auraenabled
    public static void skillrating(string caseid,string csrating)
    {
        System.debug('skillrating--'+csrating);
        list<case> cs = [select id,skill_rating__c from case where id=:caseid limit 1]; 
        cs[0].skill_rating__c=csrating;
        update cs;
    }
      @auraenabled
    public static void techrating(string caseid,string csrating)
    {
        list<case> cs = [select id,tech_rating__c from case where id=:caseid limit 1]; 
         cs[0].tech_rating__c=csrating;
        update cs;
        
    }
      @auraenabled
     public static void duedeligence(string caseid,string csrating)
    {
        list<case> cs = [select id,due_deligence__c from case where id=:caseid limit 1]; 
         cs[0].due_deligence__c=csrating;
        update cs;
        
    }
      @auraenabled
    public static string fetchcaserating(string caseid,String starsType)
    {
        list<case> cs = [select id,caserating__c,skill_rating__c,Tech_Rating__c,due_deligence__c from case where id=:caseid limit 1]; 
        String ratingValue=null;
        if(starsType=='stars'){
        ratingValue=cs[0].caserating__c;
        }else if(starsType=='starsSkill'){
            ratingValue=cs[0].skill_rating__c;
        }else if(starsType=='starsRating'){
            ratingValue=cs[0].Tech_Rating__c;
        }
        else if(starsType=='duedeligencerating'){
            ratingValue=cs[0].due_deligence__c;
        }
        return ratingValue;
    }
    
        @auraenabled
    public static string fetchtechrating(string caseid)
    {
        list<case> cs = [select id,tech_rating__c from case where id=:caseid limit 1]; 
        return cs[0].tech_rating__c;
        
    }
}