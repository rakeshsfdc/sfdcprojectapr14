({
	saveRecord : function(component, event, helper)
    {
	  console.log('Front end method called');
      var ename = component.get("v.EmplName");
      var sal= component.get("v.Salary");
        
       var action = component.get("c.emprecord");    //preparing to call  backend method
       
       // passing inputs to the method
        // adding params to call backend method
       action.setParams({							
            Name : ename,
            Salary : sal
        });    
        
        action.setCallback(this,function(a){            // process the response from backend
            var state = a.getState();            
            if(state == "SUCCESS")
            {
                console.log('resopnse type---'+state)
                var lacc = a.getReturnValue();   
                console.log('response--'+lacc);
                // window.open('/' + lacc.Id);                  
            }else if(state == "ERROR"){
                alert('Error in calling server side action');  
            }
           
        })
		$A.enqueueAction(action);
       
      
	}
})