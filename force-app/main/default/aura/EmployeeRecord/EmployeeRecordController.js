({
	doInit : function(component, event, helper) 
    {
		var action = component.get("c.countryvalues");  
       // action.setParams({accname: component.get("v.accname")})
        action.setCallback(this,function(a){            
            var state = a.getState();            
            if(state == "SUCCESS")
            {
               component.set("v.countries",a.getReturnValue()) ;                              
            }else if(state == "ERROR"){
                alert('Error in calling server side action');  
            }
        })
		$A.enqueueAction(action);

	},
    saveRecord : function(component, event, helper){
        var saveRecVals = component.get("v.Employee");
        var action = component.get("c.insertEmployee");
        action.setParams({Empl: saveRecVals})
         action.setCallback(this,function(a){            
            var state = a.getState();            
            if(state == "SUCCESS")
            {
               component.set("v.Employee",a.getReturnValue()) ;
                var inputArray = [];
                var outputArray = [];
                inputArray = component.find("inputValCls");
                for(var cmp in inputArray) {
                    $A.util.removeClass(inputArray[cmp], "showValCls");
                    $A.util.addClass(inputArray[cmp], "hideValCls");
                }
                
                outputArray = component.find("outputValCls");
                for(var cmp in outputArray) {
                    $A.util.removeClass(outputArray[cmp], "hideValCls");
                    $A.util.addClass(outputArray[cmp], "showValCls");
                }
            }else if(state == "ERROR"){
                alert('Error in calling server side action');  
            }
        })
		$A.enqueueAction(action);
    },
    canceEditRecord : function(component, event, helper){
        var inputArray = [];
        var outputArray = [];
        inputArray = component.find("inputValCls");
        for(var cmp in inputArray) {
            $A.util.removeClass(inputArray[cmp], "showValCls");
            $A.util.addClass(inputArray[cmp], "hideValCls");
        }
        
        outputArray = component.find("outputValCls");
        for(var cmp in outputArray) {
            $A.util.removeClass(outputArray[cmp], "hideValCls");
            $A.util.addClass(outputArray[cmp], "showValCls");
        }
    },
    editRecord : function(component, event, helper){
        var inputArray = [];
        var outputArray = [];
        inputArray = component.find("inputValCls");
        for(var cmp in inputArray) {
            $A.util.removeClass(inputArray[cmp], "showValCls");
            $A.util.addClass(inputArray[cmp], "hideValCls");
        }
        
        outputArray = component.find("outputValCls");
        for(var cmp in outputArray) {
            $A.util.removeClass(outputArray[cmp], "hideValCls");
            $A.util.addClass(outputArray[cmp], "showValCls");
        }
    }
})