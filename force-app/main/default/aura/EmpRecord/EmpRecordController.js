({
	doInit : function(component, event, helper) {
		alert('Init');
        var action = component.get("c.countryvalues");
        action.setCallback(this,function(a){ 
            alert('client js');
            var state = a.getState();            
            if(state == "SUCCESS")
            {
                var res = a.getReturnValue();
                component.set('v.countryvalues',res);
               
                //alert('plv--->'+JSON.stringify(res));
				//component.set("v.Employee",a.getReturnValue()) ;  
				                            
            }else if(state == "ERROR"){
                alert('Error in calling server side action');  
            }
        })
        $A.enqueueAction(action);        
	},
    saverecord :function(component,event,helper){
        alert('save method initiated');
        var action=component.get("c.saveEmployee");
        action.setParams({emp:component.get('v.Employee')})
        action.setCallback(this,function(a){             
            var state = a.getState();            
            if(state == "SUCCESS")
            {              
               component.set("v.status",'Record inserted'+a.getReturnValue()) ; 
              
            }else if(state == "ERROR"){
                alert('Error in calling server side action');  
            }
        })
        $A.enqueueAction(action); 
    }
})