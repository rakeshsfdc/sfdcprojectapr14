({
	doInit : function(component, event, helper) {
         var accid=component.get("v.recordId");
       var action= component.get("c.relatedconmethd");
        action.setParams({
                        
            acc:accid

        });
         action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.conlist", response.getReturnValue());
                var relrec=component.get('v.conlist[0]');
                var count=component.get('v.conlist.length');
                console.log(relrec);
                component.set('v.relcon',relrec);
                component.set('v.concount',count);
            }
         });
         $A.enqueueAction(action); 

            
        
        
		
	},
    
    popupmethd: function(com,evt,helper)
    {
        com.set('v.popup',true);
    },
    popupclose:function(com,evt,helper)
    {
            com.set('v.popup',false);

}
})