trigger CaseTrigger on Case (after update,before update)
 {
     
    CaseHandler ch = new CaseHandler();
    if(trigger.isafter && trigger.isupdate)
    {
        if(RecursiveHandler.runonce())
        {
        	ch.updateChildCases(trigger.new,trigger.newmap);
        }
    }
     
     if(trigger.isbefore && trigger.isupdate)
     {
        ch.oldownername(trigger.new,trigger.oldmap,trigger.newmap);
         
     }
        
  }