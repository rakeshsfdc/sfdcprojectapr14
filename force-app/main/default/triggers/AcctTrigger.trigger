trigger AcctTrigger on Account (before insert,before update,after update, before delete,after insert,after delete)
{    
    AcctTriggerHandler ath = new AcctTriggerHandler();  
    
    
    if(trigger.isbefore && trigger.isinsert)
    {
          ath.defaultBillingCountry(Trigger.new);// list<account>   
         // ath.uniqueaccountrecs(Trigger.new);
    }
    
    if(trigger.isbefore && trigger.isdelete)
    {
        ath.validatePhone(trigger.old);//list<account>
    }
    
    if(trigger.isafter && trigger.isinsert)
    {
        ath.insertRelatedData(trigger.new);//list<account>
    }
    
    if(trigger.isbefore && trigger.isupdate)
    {
     // ath.validatebillingcountry(trigger.new,trigger.oldmap);
    }
    
    if(trigger.isafter && trigger.isupdate)
    {
       
       ath.rollupaction(Trigger.newmap);
    }
    
}